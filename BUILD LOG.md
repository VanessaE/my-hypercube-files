**BUILD LOG**

This project was started on 2019-12-03, and essentially completed on 2020-02-04.

It stands 500mm tall (plus the green/black feet) and should have a ~200 x 197 x 275 mm build volume.

X uses 10 mm OD/8 mm ID carbon fibre tubes, Y uses 8 mm chromed steel rods, and Z uses 12 mm chromed steel rods.

I'm using Misumi Series-5 2020 extrusion; that which is depicted here was derived from the technical drawing for that product.

Scroll to the bottom of this page to see the current photos of the printer.

![](Images/Render.png)

The base machine:  https://www.thingiverse.com/thing:1752766

I'll be adding/using the following mods:

* The piece of 2020 going across under the bed provides a place for the power supplies' middle-facing sides to attach to, and for the front of the bed to rest on when it's parked at the bottom of the print volume.
* A simple "pad" and two spacers (made by me) to attach to the crossbar and rear, lower X bar, so that the bed frame will sit level when parked, and won't make metal-on-metal contact on the crossbar.
* Stronger 8mm Y rod mounts: https://www.thingiverse.com/thing:2169166 
* XY motor angle adjusters: https://www.thingiverse.com/thing:2036245
* PSU mounting bracket: https://www.thingiverse.com/thing:2467824 (but significantly reduced in size, plus modified versions to attach the inner sides of the two PSUs to that extra X bar under the bed)
* Bed mounting brackets:  https://www.thingiverse.com/thing:2471033 (plus some standoffs since I don't use bed springs)
* Corner brackets for the middle/back of the bed carriage:  https://www.thingiverse.com/thing:2740948 (modified by me to be much thicker)
* X carriage:  https://www.thingiverse.com/thing:2374167  (further modified by me to accept dual LM10UU bushings on top rather than an LM12LUU, and to alter the endstop mount position/angle)
* Z bearing mounts:  https://www.thingiverse.com/thing:2392973 (because I want to use regular LM12UU bearings, not long LM12LUU)
* The nameplate:  https://www.thingiverse.com/thing:2087379
* The pink LCD case at the top right and the green and white controller cast at the rear, lower right are by me, and are derived from my generic BigTreeTech SKR v1.x/RRD 12864 case:  https://www.prusaprinters.org/prints/7686-case-enclosure-for-skr-and-rrd-12864-display
 * The extruder is my Geared B'struder, https://www.prusaprinters.org/prints/7456-gregs-bwadestruder-geared-bstruder-bowden-extruder 
* The power switch mount is a customized version of the one I made for LACK table enclosures, https://www.prusaprinters.org/prints/7478-lack-table-power-switch-mount (incorporating a rocker switch for the mains, and a push-on-push-off switch for the lighting)
* The 24v PSU cover is a my own design, with a hole for the mains wire and an inset female XT90 connector for the 24v side.
* The mains socket mount on the back of the machine is something I threw together just for this machine.
* Ditto for the spool holder.
* not shown in the render are printed 2020 end-caps and slot covers, plus whatever bits I'll need to come up with for cable management.
* I've also added a mount for a 688ZZ bearing at the top of the lead screw, as a way of protecting it from lateral movement, since it's so long.

A bit of credit: Many of the items shown in this render are pulled from my "Standard Library" Blender file, which in turn came from various sources. See https://www.prusaprinters.org/prints/16640-my-blender-3d-printing-partsmaterials-library for that library file.

**2019-12-03**

I took a photo of my Prusa i3's display, filtered the living hell out of the pic to reduce it to a clean, pixel-accurate screenshot, then edited my custom logo seen in the upper-left to replace the "Prusa i3" that appeared there with a pixel-art tesseract (which is one form of a hypercube in geometry).

![](Images/12864_screenshot__HC.png)

This one is just the more-complete, 20x-scaled and softened "simulated photo" that's used on the LCD in the render at the top of this page.

![](Images/12864_simulated_photo__hypercube.png)

**2019-12-04**

Test prints of some short 2020 segments to get a feel for tapping and otherwise using this stuff, and showing the joining method the frame will use.

![](Images/P1180467.JPG)

**2019-12-06**

The real thing, all drilled and tapped.

![](Images/P1180482.JPG)

**2019-12-12**

Outer frame assembled and ready to use, plus some of the printed parts.

![](Images/P1180485.JPG)

**2019-12-13**

The carbon fibre tubes and some of the Hypercube's printed parts.  Dog not included :stuck_out_tongue: 

![](Images/P1180487.JPG)

**2019-12-14**

Test fitting the X gantry (with my modified carriage).  It slides smooth and easily - just barely by its own mass/gravity alone.  Took a few tries to get the drylin bushings on the X axis preloaded just right.  I decided to go with regular LM8UU linear bearings for the Y axis.

![](Images/P1180489.JPG)

**2019-12-21**

Nameplate and some of the slot covers added, and testing the 12v supply and lighting.  Of the two cords going out of frame on the left, the whiter one will eventually feed the 24v supply.  The two switches in the foreground are for the lights (left) and mains (right, will handle both power supplies).

And no, cyan (actually "Aqua") wasn't my first choice for the power supply enclosure, but I don't have any or enough of any other more appropriate color of PETG or ABS.

![](Images/P1180493.JPG)

**2019-12-22**

My Y rods arrived in the mail... or one of them did anyway.  The seller shipped them in a plastic bag filled with padding, instead of a proper shipping tube.... naturally, the bag ripped in transit and one of them fell out and got lost, for which I received a full refund (I was only counting on 50%).  The one I did get is in perfect condition (clean, round, perfectly straight), despite the shipping damage.....

![](Images/P1180495.JPG)

.....I still needed another rod, so I dug out my spares (leftovers from when I put new rods on my Prusa i3 a while back).  As it happens, two of them were still brand new (they never got put into service in the i3 because I had ordered the wrong length).   They were a few centimeters too long, but 15 minutes and 20% of a Dremel cutting disc later and I got one cut to just the right length.  I won't win any machinist awards with my cutting and grinding skills, but at least I flattened the end decently, and I even beveled the edge. :smile:

![](Images/P1180496.JPG)

Y rods ready to use... as soon as the screws and T-nuts arrive so that I can attach the mounts to the frame.

![](Images/P1180497.JPG)

**2020-01-02**

Installing the middle support rail and 12v power supply (for the LED lighting)

![](Images/P1180498.JPG)

Added the rear mains socket and front power switches, wired things up, and bingo.  We have lighting.  Also added some more slot covers where practical.

![](Images/P1180501.JPG)

**2020-01-03**

XY gantry installed and ready to use.  Both axes move smoothly, and it'll fall under its own (low) mass along either axis.  To ensure squareness, I used a bit of scrap 2020 as a gauge while installing the Y rod mounts, putting all 4 ends precisely 20 mm below the frame Y bars.  You may notice I only put one bearing in the left XY joiner - this is to increase Y travel, as 2 bearings would hit either the end stop, or the rear Y rod mount, well before reaching the limits of the XY joiners' travel.

I also took the time to install the corner belt idlers.

Why are the pulleys black, you ask?  Because I made my own, which fit over normal 623ZZ bearings (based on the ones commonly used on Prusa i3's). What?  I didn't wanna spend the money. :smiley:

EDIT: those printed flanges didn't work out, they kept rubbing on ...everything they could, making them far too tight to work, so I ordered the proper flanged F623ZZ's.

![](Images/P1180502.JPG)

The Z/bed carriage assembled and ready to install... as soon as I have something to install it *on* (Z rods are still in the mail).

![](Images/P1180500.JPG)

**2020-01-10**

Test-fitting the extruder (my geared B'struder remix) and filament roller (a simple cylinder with flanged ends, fitted with 688ZZ bearings, the middles of which rest on pegs mounted on the left end and behind the spool.  Yes, 688, not 608 -- I don't choose bearings based on others' common uses.  :stuck_out_tongue: 

![](Images/P1180508.JPG)

**2020-01-11**

Z axis and bed carriage installed.

![](Images/P1180512.JPG)

**2020-01-13**

Installed the bed and lead screw.  Had to alter and re-print the bed mounts, the nut traps were sized wrong.

![](Images/P1180513.JPG)

**2020-01-19**

Installed the two XY motors and the Z motor, and aligned the Z lead screw.  Not shown in the pics, I took the time to install the motor wiring and the Y end stop and its wiring, using the 2020s' slots and their coverings to contain the wire.

The bit of aluminum tape on the lead screw provides a good amount of grip by way of its softness and of course its adhesive (otherwise, the lead screw would eventually slip out of the coupler).

![](Images/P1180522.JPG)
![](Images/P1180523.JPG)

Also installed the 12864 LCD, but the cabling for it will have to come later (still in the mail, plus it'll wait until I install the SKR v1.4 controller).

![](Images/P1180524.JPG)

**2020-01-26**

By altering the Z rod mounts, Z bearing/bed mounts, Z motor mount, the two rear Y rod mounts, and a few other minor parts, I was able to gain several more millimeters of Y travel.  Should be enough to allow the nozzle to comfortably reach the entire 200mm bed now.  Had to sacrifice some Z travel to do it though. :confused:

Here, the Z rod mount is basically redone from scratch.  Although the original Hypercube parts sought to keep everything on the inner surfaces of the frame, don't mind it clamping on the back and top, since I already have other stuff sticking out (like the spool holder, extruder, mains socket, ...).

To give Z enough travel for the bed's frame to reach the rests below, I had to crop out the corners of the Z bearing/bed mounts mounts, else they conflict with the frame.

By moving the motor down below the frame, and slightly rounding-off the corners of the larger of the two motor/leadscrew coupler nuts, I could move the motor rearward, carrying the leadscrew with it, to follow the re-positioned Z rods.  Two of the motor mount screws are now hidden under the frame, but they're sunk/inset into the motor mount (which is why it's so much thicker), so the motor is still fully-mounted.

I also had to thin the rear Y rod mounts so that the XY joiners could move further back.  Since the left XY joiner only has one bearing, and neither joiner can reach the front mounts (since the XY motors are there), I really only had to modify the left rear mount (I modified all four, but didn't bother to install the front two).

![](Images/P1180528.JPG)
![](Images/P1180529.JPG)
![](Images/P1180530.JPG)
![](Images/P1180532.JPG)

**2020-02-03**

Received and installed the 24v power supply

![](Images/P1180534.JPG)

SKR v1.4 all set up and ready to go

![](Images/P1180535.JPG)

**2020-02-04**

Built and installed the the effector.  RPW-Ultra (J-head 12) hotend, inductive sensor, two fans.

![](Images/P1180536.JPG)

A bit later....

**IT'S ALIVE!**

![](Images/P1180537.JPG)

**...AND IT PRINTS!!**

Although masking tape makes for a mediocre bed surface prep, it's only temporary -- my Printbite sheet is still in the mail.  That aside, the print quality is phenomenal compared to my i3 using the same settings.  There was some ooze in this print, but that's because I forgot to increase my retraction length to compensate for this being a being bowden-fed machine instead of direct.

My initial move tests before installing the effector had this thing doing almost 1100 mm/s on travels (with 2000 mm/s² acceleration) in X/Y, and 200 mm/s along Z.

The machine ended up with a final build volume of 211x200x271 mm, of which about 200x199x270 will be usable once the Printbite is applied.

There are still some minor tweaks to be done, but mainly it's down to just calibrating print settings and firmware at this point.

Project effectively completed on 2020-02-04.

This more or less concludes my build log, though I'll still post updates from time to time, if appropriate.  I'll keep https://gitlab.com/VanessaE/my-hypercube-files up-to-date as well, as I revise parts or come up with new things.

![](Images/P1180539.JPG)

**2020-02-10**

Ok, I had one more thing to do :stuck_out_tongue: 

I wasn't satisfied with the behavior and overall size of the effector, so I redesigned it.  It now uses a 40x10 fan for the cold end, and a 5015 for the layer cooler, and everything's shifted around a bit.

This regained some Y travel, but I had to sacrifice a little bit of X travel, which is fine since it 's still well over 200 mm.
Between those changes, seen here...

![](Images/P1180543.JPG)

...and the following change to the left X/Y joiner (wherein I've made the front part a bit shorter to increase the Y travel, and added a "shoulder" to make sure it always hits the end stop, along with some other clearance adjustments that are not visible here), some extra clearance on the back edge of the left motor mount, and an altered Y end stop holder to move the end stop down a bit so that the X/Y joiner always hits the lever's round bit, the machine now has a build volume of about 207x205x272 mm, all of which should be usable, aside from the extreme corners where the bed/glass clamps are (I'll still be putting a 200x200 sheet of Printbite on it, for a roughly 200x200x271 volume).

![](Images/P1180544.JPG)

**2020-03-22**

The left-side-only "minimalist" nozzle I was using wasn't working too well in that stuff sticking out to the right on a part wasn't being cooled well enough.  This ought to do the trick (inside, there is a divider and contour that "splits" the fan's airpath to try to make it hit both sides evenly).

~~Sorry it looks so rough, I don't use my Hypercube for printing ABS (no enclosure), just my old i3.  Note to self:  re-print this part with a better first layer/Z offset, and rotate the damn hotend so as not to touch it.~~  (EDIT: Re-printed after fixing the hotend... then did it again because I knew it could be better :stuck_out_tongue: )

![](Images/P1180600.JPG)

**2020-04-12**

I redesigned the extruder mount to turn it 90°, allowing me to pack more than 2 onto the left upper Y bar.  My plan, if it works out, is to go to at least 4 extruders.  Plus, this mount design uses TPU for one half (the green part), significantly reducing noise from the extruder (which is mainly from retract/unretract moves).  Don't mind the blue things, those are just filament dust filters.  I keep them on top of the extruders when not in use.

![](Images/P1180590.JPG)
![](Images/P1180591.JPG)

---

**CURRENT STATE**

Here's how the printer looks, as of 2020-04-14:

![](Images/P1180592.JPG)
![](Images/P1180593.JPG)
