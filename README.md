This repository contains all of the original Hypercube parts.

It also includes a bunch of parts I downloaded from Thingiverse, 
modified here and there, as needed.  Most of those parts are listed in 
the build log.

Many of the items referenced in the .blend are pulled from my "Standard 
Library" Blender file, which in turn came from various sources. See 
https://www.prusaprinters.org/prints/16640-my-blender-3d-printing-partsmaterials-library 
for that library file.

Finally, all of my customized parts are included, as well as a fancy 
render, some tools/jigs, and a couple other things.

You can find the full build log [HERE](BUILD LOG.md).

Here is a render of the machine (this image will be replaced from time to time as random bits of the project change or if I decide to screw around with my rendering settings :smiley: ).  Colors of the printed parts are shown as intended, not necessarily what I printed with (i.e. if I'm out of some color).

![](Images/Render.png)
